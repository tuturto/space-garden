{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings          #-}

module Main (main) where

import Import
import Run
import qualified RIO.List as L
import RIO.Process
import Options.Applicative.Simple
import qualified Paths_space_garden
import System.Directory ( listDirectory )
import System.FilePath

import SDL
import qualified SDL.Image as Image
import qualified SDL.Font as Font



main :: IO ()
main = do
  (options, ()) <- simpleOptions
    $(simpleVersion Paths_space_garden.version)
    "Space garden"
    "Game for growing your very own garden in space"
    (Options
      <$> switch ( long "verbose"
                   <> short 'v'
                   <> help "Verbose output"
                 )
      <*> switch ( long "development"
                   <> short 'd'
                   <> help "Development mode"
                 )
    )
    empty

  bracket (loadResources options)
          freeResources
          (runApplication options)


runApplication :: Options -> (Textures, Renderer, Window) -> IO ()
runApplication options (textures, renderer, _) = do
  lo <- logOptionsHandle stderr (optionsVerbose options)
  pc <- mkDefaultProcessContext
  withLogFunc lo $ \lf ->
    let app = App
          { appLogFunc = lf
          , appProcessContext = pc
          , appOptions = options
          , appRenderer = renderer
          , appTextures = textures
          }
     in runRIO app run


-- | Display a loading screen and load images and fonts from disk
loadResources :: Options -> IO (Textures, Renderer, Window)
loadResources options = do
  SDL.initializeAll
  Image.initialize [Image.InitPNG]
  Font.initialize

  cursorVisible $= False

  window <- createWindow "Space Garden" (defaultWindow { windowInitialSize = if optionsDevelopment options
                                                                                then V2 1280 720
                                                                                else V2 1920 1080
                                                       , windowBorder = optionsDevelopment options
                                                       })
  renderer <- createRenderer window (-1) defaultRenderer
  rendererLogicalSize renderer $= Just (V2 960 540) -- 60 * 33 tiles (16x16)  
  font <- Font.load "assets/textures/Pixel64_v1.2.ttf" 40
  
  clear renderer
  rendererDrawColor renderer $= V4 0 0 0 255
  loadingSurface <- Font.solid font (V4 250 250 250 255) "Loading..."
  loading <- createTextureFromSurface renderer loadingSurface
  loadingInfo <- queryTexture loading
  let w = textureWidth loadingInfo
  let h = textureHeight loadingInfo
  copy renderer loading Nothing (Just (Rectangle (P $ V2 (480 - (w `div` 2)) (270 - (w `div` 2))) (V2 w h)))
  present renderer

  starsTexture <- Image.loadTexture renderer "assets/textures/stars.png"
  tileTextures <- loadTileTextures renderer
  playerTexture <- Image.loadTexture renderer "assets/textures/sprite-player.png"
  sprayTexture <- Image.loadTexture renderer "assets/textures/spray.png"
  freeSurface loadingSurface
  destroyTexture loading

  return ( Textures { _texturesStars = starsTexture
                    , _texturesTiles = tileTextures
                    , _texturesPlayer = playerTexture
                    , _texturesFont = font
                    , _texturesSpray = sprayTexture
                    }
          , renderer
          , window )


-- | Free textures and fonts used by program
freeResources :: (Textures, Renderer, Window) -> IO ()
freeResources (textures, _, window) = do
  mapM_ (\n -> destroyTexture $ n textures)
        [ _texturesStars
        , _texturesPlayer
        , _texturesSpray
        ]
  mapM_ (\(_, n) -> destroyTexture n) (textures ^. texturesTiles)
  destroyWindow window
  Font.quit
  Image.quit
  SDL.quit


loadTileTextures :: Renderer -> IO [(FilePath, Texture)]
loadTileTextures renderer = do
  files <- liftIO $ listDirectory "assets/tilesets/"
  let textureFiles = L.filter (\f -> takeExtension f == ".png") files

  mapM (\f -> do
              texture <- Image.loadTexture renderer ("assets/tilesets/" <> f)
              return (f, texture))
       textureFiles
