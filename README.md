# space-garden

## Execute  

* Run `stack exec -- space-garden-gui` to start the game
* With `stack exec -- space-garden-gui --verbose` you will see the same message, with more logging.

## Run tests

`stack test`
