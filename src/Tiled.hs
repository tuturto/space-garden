{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE OverloadedStrings          #-}

module Tiled where

import RIO

import Data.Aeson ( (.:), (.:?), FromJSON(..), withObject )
import qualified Data.List as L'
import qualified Data.Vector.Unboxed as U
import Lens.Micro.TH ( makeLenses )

data TiledMap = TiledMap
  { _tiledMapWidth      :: !Int
  , _tiledMapHeight     :: !Int
  , _tiledMapLayers     :: ![TiledLayer]
  , _tiledMapTilesets   :: ![TilesetLink]
  }

instance FromJSON TiledMap where
  parseJSON = withObject "tiled map" $ \o -> do
    width <- o .: "width"
    height <- o .: "height"
    layers <- o .: "layers"
    tilesets <- o .: "tilesets"

    return $ TiledMap
              { _tiledMapWidth = width
              , _tiledMapHeight = height
              , _tiledMapLayers = layers
              , _tiledMapTilesets = tilesets
              }


data TilesetLink = TilesetLink
  { _tilesetLinkFirstGid :: !Int
  -- | json source of tileset
  , _tilesetLinkSource   :: !Text
  }

instance FromJSON TilesetLink where
  parseJSON = withObject "tileset" $ \o -> do
    firstGid <- o .: "firstgid"
    source <- o .: "source"

    return $ TilesetLink
              { _tilesetLinkFirstGid = firstGid
              , _tilesetLinkSource = source
              }


data TiledLayer = TiledLayer
  { _tiledLayerName   :: !Text
  , _tiledLayerWidth  :: !Int
  , _tiledLayerHeight :: !Int
  , _tiledLayerData   :: !(U.Vector Int)
  }

instance FromJSON TiledLayer where
  parseJSON = withObject "tiled layer" $ \o -> do
    name <- o .: "name"
    widthM <- o .:? "width"
    heightM <- o .:? "height"
    tiles <- o .:? "data"

    let width = maybe 0 id widthM
    let height = maybe 0 id heightM
    
    let uTiles = U.generate (width * height) ((maybe [] id tiles) L'.!!)

    return $ TiledLayer
              { _tiledLayerName = name
              , _tiledLayerData = uTiles 
              , _tiledLayerWidth = width
              , _tiledLayerHeight = height
              }


data Tileset = Tileset
  { _tilesetColumns   :: !Int
  , _tilesetImage     :: !Text
  , _tilesetName      :: !Text
  , _tilesetTileCount :: !Int
  }

instance FromJSON Tileset where
  parseJSON = withObject "tileset" $ \o -> do
    columns <- o .: "columns"
    image <- o .: "image"
    name <- o .: "name"
    tileCount <- o .: "tilecount"

    return $ Tileset
              { _tilesetColumns = columns
              , _tilesetImage = image
              , _tilesetName = name
              , _tilesetTileCount = tileCount
              }


makeLenses ''TiledMap
makeLenses ''TiledLayer
makeLenses ''TilesetLink
makeLenses ''Tileset
