{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}

module UserInterface (render, handleEvents, update, setupParallax)
  where

import RIO
import RIO.Lens (_1, _2 )
import qualified RIO.Partial as RIO'
import qualified RIO.List.Partial as L'
import Lens.Micro ( (+~) )
import Control.Monad ( replicateM )
import Control.Monad.Random ( RandomGen, Rand, getRandomR, evalRandIO, getRandomR )
import SDL
import qualified SDL.Font as Font

import Data.Fixed

import qualified Data.List as L
import qualified Data.Vector.Unboxed as U

import Foreign.C.Types
import Types

playerSpeed :: Double -- speed in pixels / ms
playerSpeed = 0.048

-- | Hard coded values for some of the special tiles
emptyTile :: Int
emptyTile = 0
risingRamp :: Int
risingRamp = 27
fallingRamp :: Int
fallingRamp = 25

render :: GameState -> RIO App ()
render state =
  case state of
    MainMenu stateData ->
      renderMainMenu stateData

    GameRunning stateData ->
      renderGameRunning stateData

    GameExiting ->
      return ()


handleEvents :: GameState -> Event -> RIO App GameState
handleEvents state event =
  if quitRequested event
    then
      return GameExiting

    else (
      case state of
        MainMenu stateData ->
          handleEventsMainMenu stateData event

        GameRunning stateData ->
          handleEventsGameRunning stateData event

        GameExiting ->
          return GameExiting)


update :: Word32 -> GameState -> RIO App GameState
update t state =
  case state of
    MainMenu stateData ->
      updateMainMenu t stateData

    GameRunning stateData ->
      updateGameRunning t stateData

    GameExiting ->
      return GameExiting


keyEvent :: InputMotion -> Event -> Keycode -> Bool
keyEvent motion event code =
  case eventPayload event of
    KeyboardEvent eventData ->
      keyboardEventKeyMotion eventData == motion
        && not (keyboardEventRepeat eventData)
        && (keysymKeycode . keyboardEventKeysym) eventData == code

    _ ->
      False


keyPressed :: Event -> Keycode -> Bool
keyPressed = keyEvent Pressed


keyReleased :: Event -> Keycode -> Bool
keyReleased = keyEvent Released


handleEventsMainMenu :: MainMenuData -> Event -> RIO App GameState
handleEventsMainMenu menuData event
  | keyPressed event KeycodeEscape = do
      return GameExiting

  | keyPressed event KeycodeSpace = do
      return $ newGame menuData $ eventTimestamp event

  | otherwise = do
    return $ MainMenu menuData


newGame :: MainMenuData -> Word32 -> GameState
newGame menuData tick =
  GameRunning $ GameData
  { _gameDataParallax       = menuData ^. mainMenuDataParallax
  , _gameDataTick           = tick
  , _gameDataBackgroundTick = menuData ^. mainMenuDataBackgroundTick
  , _gameDataPlayer         = startingPlayer tick
  , _gameDataMap            = menuData ^. mainMenuDataMaps . defaultMapsGame
  , _gameDataDefaultMaps    = menuData ^. mainMenuDataMaps
  , _gameDataControlState   = initialControlState
  }


initialControlState :: ControlState
initialControlState =
  ControlState
    { _controlStateRight = False
    , _controlStateLeft  = False
    , _controlStateUp    = False
    , _controlStateDown  = False
    , _controlStateWater = False
    }


startingPlayer :: Word32 -> Player
startingPlayer tick =
  Player
    { _playerLocation    = (262, 79)
    , _playerState       = StandingRight
    , _playerAnimStarted = tick
    }


updateGameRunning :: Word32 -> GameData -> RIO App GameState
updateGameRunning t gameData = do
  parallax' <- liftIO $ evalRandIO $ updateParallax dt (_gameDataParallax gameData)
  step1 <- liftIO $ evalRandIO $ simulate t gameData

  return $ GameRunning $ step1 & gameDataBackgroundTick .~ t
                               & gameDataTick .~ t
                               & gameDataParallax .~ parallax'
  where
    dt = t - gameData ^. gameDataBackgroundTick    


updateMainMenu :: Word32 -> MainMenuData -> RIO App GameState
updateMainMenu t menuData = do
  parallax' <- liftIO $ evalRandIO $ updateParallax dt (_mainMenuDataParallax menuData)

  return $ MainMenu $ menuData & mainMenuDataBackgroundTick .~ t
                               & mainMenuDataParallax .~ parallax'
  where
    dt = t - menuData ^. mainMenuDataBackgroundTick


handleEventsGameRunning :: GameData -> Event -> RIO App GameState
handleEventsGameRunning gameData event
  | keyPressed event KeycodeEscape =
    return $ backToMenu gameData $ eventTimestamp event

  | keyPressed event KeycodeRight = do
      res <- liftIO $ evalRandIO $       
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateRight .~ True)
      return $ GameRunning res

  | keyReleased event KeycodeRight = do
      res <- liftIO $ evalRandIO $ 
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateRight .~ False)
      return $ GameRunning res

  | keyPressed event KeycodeDown = do
      res <- liftIO $ evalRandIO $ 
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateDown .~ True)
      return $ GameRunning res
      

  | keyReleased event KeycodeDown = do
      res <- liftIO $ evalRandIO $ 
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateDown .~ False)
      return $ GameRunning res
      

  | keyPressed event KeycodeLeft = do
      res <- liftIO $ evalRandIO $
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateLeft .~ True)
      return $ GameRunning res
      

  | keyReleased event KeycodeLeft = do
      res <- liftIO $ evalRandIO $
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateLeft .~ False)
      return $ GameRunning res
      

  | keyPressed event KeycodeUp = do
      res <- liftIO $ evalRandIO $
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateUp .~ True)
      return $ GameRunning res
      

  | keyReleased event KeycodeUp = do
      res <- liftIO $ evalRandIO $
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateUp .~ False)
      return $ GameRunning res
      

  | keyPressed event KeycodeSpace = do
      res <- liftIO $ evalRandIO $
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateWater .~ True)
      return $ GameRunning res
      

  | keyReleased event KeycodeSpace = do
      res <- liftIO $ evalRandIO $
                simulate (eventTimestamp event)
                         (gameData & gameDataControlState . controlStateWater .~ False)
      return $ GameRunning res
      

  | otherwise =
    return $ GameRunning gameData


simulate :: (RandomGen g) => Word32 -> GameData -> Rand g GameData
simulate tick gameData = do
  let dd = desiredDirection $ gameData ^. gameDataControlState
  let dt = if tick >= gameData ^. gameDataTick
              then fromIntegral $ tick - gameData ^. gameDataTick
              else 0
  (x, y, pState, level) <- movePlayer gameData dd dt
  let animStart = if playerStateChanged (gameData ^. gameDataPlayer . playerState) pState
                  then
                    tick
                  else
                    gameData ^. gameDataPlayer . playerAnimStarted
  return $ gameData & gameDataPlayer . playerLocation .~ popPlayer (gameData ^. gameDataMap) (x, y)
                    & gameDataPlayer . playerState .~ pState
                    & gameDataPlayer . playerAnimStarted .~ animStart
                    & gameDataTick .~ tick
                    & gameDataMap .~ level


-- | Make sure player doesn't sink into ground
popPlayer :: Level -> (Double, Double) -> (Double, Double)
popPlayer level (x, y)
  | not (tileAt level xd yd `elem` [emptyTile, risingRamp, fallingRamp]) =
      (x, fromIntegral yd * 16 - 1)

  | otherwise =
      (x, y)

  where
    xd = x `div'` 16
    yd = y `div'` 16


playerStateChanged :: PlayerState -> PlayerState -> Bool
playerStateChanged old new =
  case (old, new) of
    (StandingLeft, StandingLeft) -> False
    (StandingLeft, _) -> True
    (StandingRight, StandingRight) -> False
    (StandingRight, _) -> True
    (Walking dir0, Walking dir1) -> dir0 /= dir1
    (Walking _, _) -> True
    (Climbing dir0, Climbing dir1) -> dir0 /= dir1
    (Climbing _, _) -> True
    (Falling _, Falling _) -> False
    (Falling _, _) -> True
    (IdleFall, IdleFall) -> False
    (IdleFall, _) -> True
    (Sitting, Sitting) -> False
    (Sitting, _) -> True
    (Watering d0, Watering d1) -> d0 /= d1
    (Watering _, _) -> True


desiredDirection :: ControlState -> (Direction, Bool)
desiredDirection state =
  (Direction yDir xDir, state ^. controlStateWater)
  where
    xDir = case (state ^. controlStateWater, state ^. controlStateLeft, state ^. controlStateRight) of
            (True, _, _)          -> Nothing
            (False, True, True)   -> Nothing
            (False, True, False)  -> Just ToLeft
            (False, False, True)  -> Just ToRight
            (False, False, False) -> Nothing
    yDir = case (state ^. controlStateWater, state ^. controlStateUp, state ^. controlStateDown) of
            (True, _, _)          -> Nothing
            (False, True, True)   -> Nothing
            (False, True, False)  -> Just ToUp
            (False, False, True)  -> Just ToDown
            (False, False, False) -> Nothing


-- compute new location and state for the player
-- state -> direction -> dt -> new location and state
movePlayer :: (RandomGen g) => GameData -> (Direction, Bool) -> Double -> Rand g (Double, Double, PlayerState, Level)
movePlayer state ((Direction vDir hDir), watering) dt
  -- keep on falling
  | playerCanFall state =
      return $ fall state dt

  | watering 
    && not (ladderAt (state ^. gameDataMap) 
                     (div' (state ^. gameDataPlayer . playerLocation . _1) 16)
                     (div' (state ^. gameDataPlayer . playerLocation . _2) 16)) =
    water state

  -- climb up or down
  | isJust vDir
    && not watering
    && playerCanClimb state (RIO'.fromJust vDir) =
      return $ climb state (RIO'.fromJust vDir) dt

  -- walk sideways or on ramp
  | isJust hDir 
    && not watering =
      return $ walk state (RIO'.fromJust hDir) dt

  -- switch to idle after fall
  | not (playerCanFall state)
    && not watering
    && state ^. gameDataPlayer . playerState . to playerStateIsFall =
      return ( state ^. gameDataPlayer . playerLocation . _1
             , state ^. gameDataPlayer . playerLocation . _2
             , if fellFromHigh state
                 then
                   Sitting

                 else
                   IdleFall
             , state ^. gameDataMap
             )

  -- stand still
  | isNothing hDir  
    && isNothing vDir 
    && not watering =
      return $ standStill state

  -- change nothing
  | otherwise =
      return ( state ^. gameDataPlayer . playerLocation . _1
             , state ^. gameDataPlayer . playerLocation . _2
             , state ^. gameDataPlayer . playerState
             , state ^. gameDataMap
             )


fellFromHigh :: GameData -> Bool
fellFromHigh state =
  case state ^. gameDataPlayer . playerState of
    Falling start ->
      state ^. gameDataPlayer . playerLocation . _2 - start >= 32

    _ ->
      False


playerStateIsFall :: PlayerState -> Bool
playerStateIsFall (Falling _) = True
playerStateIsFall _ = False


playerCanFall :: GameData -> Bool
playerCanFall state
  | tileAt level xd yd == 0
    && not (ladderAt level xd (yd))
    && ym < 15 =
      True
  
  | tileAt level xd (yd + 1) == 0
    && not (ladderAt level xd (yd + 1))
    && ym >= 15 =
      True

  | otherwise = 
      False

  where
    (x, y) = state ^. gameDataPlayer . playerLocation
    xd = x `div'` 16
    (yd, ym) = y `divMod'` 16
    level = state ^. gameDataMap


playerCanClimb :: GameData -> VDirection -> Bool
playerCanClimb state direction
  | ym >= 15
      && direction == ToDown
      && ladderAt (state ^. gameDataMap) xd (yd + 1) =
        True

  | ym < 1
      && direction == ToUp
      && ladderAt (state ^. gameDataMap) xd (yd - 1) =
        True

  | ladderAt (state ^. gameDataMap) xd yd =
      True

  | otherwise =
      False

  where
    xd = (state ^. gameDataPlayer . playerLocation . _1) `div'` 16
    (yd, ym) = (state ^. gameDataPlayer . playerLocation . _2) `divMod'` 16


walk :: GameData -> HDirection -> Double -> (Double, Double, PlayerState, Level)
walk state dir dt =
  -- compute new direction and new location
  -- check for collisions with walls
  --   if collision, move back to point where collision does not occur
  -- check for ramps
  --   if ramp, move upwards or downwards
  -- check for falling
  --   if falling, change to falling state
  ( x
  , y
  , anim
  , state ^. gameDataMap
  )
  where
    newPosition = ( state ^. gameDataPlayer . playerLocation . _1 + dx * playerSpeed * dt
                  , state ^. gameDataPlayer . playerLocation . _2
                  )
    ((x, y), anim)
      | walkingOnRamp (state ^. gameDataMap) newPosition =
          ( rampAdjustedLocation (state ^. gameDataMap) newPosition
          , Walking dir
          )

      | leavingLadder state newPosition =
          ( ladderAdjustedLocation newPosition
          , Walking dir
          )

      -- walking down a ramp
      | walkingOnRamp (state ^. gameDataMap) (newPosition & _2 +~ 1) =
          ( rampAdjustedLocation (state ^. gameDataMap) (newPosition & _2 +~ 1)
          , Walking dir
          )

      | collidesWithWall (state ^. gameDataMap) newPosition =
           -- just don't move if collision occurs, it's good enough for now
          ( state ^. gameDataPlayer . playerLocation
          , Walking dir
          )

      | otherwise =
          ( newPosition
          , Walking dir
          )

    dx = case dir of
            ToLeft ->
              -1

            ToRight ->
              1


leavingLadder :: GameData -> (Double, Double) -> Bool
leavingLadder state (newX, newY) =
  ladderAt level oxd oyd
    && not (ladderAt level nxd nyd)
    && not ((tileAt level nxd nyd) `elem` [emptyTile, risingRamp, fallingRamp])
    && tileAt level nxd (nyd - 1) == emptyTile
    && nym < 8
  where
    level = state ^. gameDataMap 
    (oldX, oldY) = state ^. gameDataPlayer . playerLocation
    oxd = oldX `div'` 16
    oyd = oldY `div'` 16
    nxd = newX `div'` 16
    (nyd, nym) = newY `divMod'` 16


ladderAdjustedLocation :: (Double, Double) -> (Double, Double)
ladderAdjustedLocation (x, y) =
  (x, fromIntegral $ yd * 16 - 1)
  where
    yd = y `div'` 16


tileAt :: Level -> Int -> Int -> Int
tileAt level x y =
  (_levelForeground level) U.! (x + y * 60)


tileAtD :: Level -> Double -> Double -> Int
tileAtD level x y =
  (_levelForeground level) U.! (fx + fy * 60)
  where
    fx = x `div'` 16
    fy = y `div'` 16


ladderAt :: Level -> Int -> Int -> Bool
ladderAt level x y =
  (_levelLadders level) U.! (x + y * 60) /= 0
    && tileAt level x y == 0


plantAt :: Level -> Int -> Int -> Int
plantAt level x y =
  (_levelPlants level) U.! (x + y * 60)


grassAt :: Level -> Int -> Int -> Int
grassAt level x y =
  (_levelGrass level) U.! (x + y * 60)


walkingOnRamp :: Level -> (Double, Double) -> Bool
walkingOnRamp level (x, y) =
  (tileAtD level x y) `elem` [risingRamp, fallingRamp]



rampAdjustedLocation :: Level -> (Double, Double) -> (Double, Double)
rampAdjustedLocation level (x, y)
  | tileAtD level x y == risingRamp =
      ( x
      , fromIntegral $ (yd * 16) + 14 - rampOffset risingRamp xm
      )

  | tileAtD level x y == fallingRamp =
      ( x
      , fromIntegral $ (yd * 16) + rampOffset fallingRamp xm
      )

  | otherwise =    
      ( x, y )
  where
      xm = x `mod'` 16
      yd = y `div'` 16


rampOffset :: Int -> Double -> Int
rampOffset fallingRamp x
  | x < 1 = -1
  | x > 14 = 15
  | otherwise = truncate x


rampOffset risingRamp x
  | x < 1 = 15
  | x > 14 = -1
  | otherwise = 15 - truncate x


rampOffset _ _ = 0


collidesWithWall :: Level -> (Double, Double) -> Bool
collidesWithWall level (x, y) =
  any (\(tx, ty) -> layer U.! (tx + ty * 60) `notElem` [emptyTile, risingRamp, fallingRamp]) locations
  where
    layer = level ^. levelForeground
    xd = x `div'` 16
    yd = y `div'` 16
    locations = [ (xd, yd)
                , (xd, yd - 1)
                ]


climb :: GameData -> VDirection -> Double -> (Double, Double, PlayerState, Level)
climb state dir dt =
  ( (fromIntegral xd) * 16 + 8
  , clampStairs state dir newY
  , Climbing $ Just dir
  , state ^. gameDataMap
  )
  where
    newY = state ^. gameDataPlayer . playerLocation . _2 + dy * playerSpeed * dt
    xd = (state ^. gameDataPlayer . playerLocation . _1) `div'` 16
    dy = case dir of
          ToUp ->
            -1

          ToDown ->
            1


clampStairs :: GameData -> VDirection -> Double -> Double
clampStairs state dir y
  | ladderAt level xd nyd =
    y

  | tileAt level xd nyd /= 0
    && dir == ToDown =
      fromIntegral $ yd * 16 + 15

  | tileAt level xd nyd == 0
    && ladderAt level xd nyd == False
    && dir == ToUp =
      fromIntegral $ nyd * 16 + 15

  | otherwise = y

  where
    level = state ^. gameDataMap
    xd = (state ^. gameDataPlayer . playerLocation . _1) `div'` 16
    yd = (state ^. gameDataPlayer . playerLocation . _2) `div'` 16
    nyd = y `div'` 16


fall :: GameData -> Double -> (Double, Double, PlayerState, Level)
fall state dt
  | newY >= 16 * 32 =
      ( startingPlayer 0 ^. playerLocation . _1
      , startingPlayer 0 ^. playerLocation . _2
      , startingPlayer 0 ^. playerState
      , state ^. gameDataMap
      )

  | tileAt level xd yd == 0 =
      ( state ^. gameDataPlayer . playerLocation . _1
      , newY
      , Falling $ case state ^. gameDataPlayer . playerState of
                        Falling y ->
                            y
                  
                        _ ->
                            state ^. gameDataPlayer . playerLocation . _2
      , state ^. gameDataMap
      )

  | otherwise =
      ( state ^. gameDataPlayer . playerLocation . _1
      , fromIntegral (yd * 16) - 1
      , StandingRight
      , state ^. gameDataMap
      )
  where
    level = state ^. gameDataMap
    newY = state ^. gameDataPlayer . playerLocation . _2 + playerSpeed * 3 * dt
    xd = state ^. gameDataPlayer . playerLocation . _1 . to (div' 16)
    yd = newY `div'` 16


standStill :: GameData -> (Double, Double, PlayerState, Level)
standStill state =
  ( state ^. gameDataPlayer . playerLocation . _1
  , state ^. gameDataPlayer . playerLocation . _2
  , case state ^. gameDataPlayer . playerState of
      StandingLeft ->
        StandingLeft

      StandingRight ->
        StandingRight

      Walking ToLeft ->
        StandingLeft

      Walking ToRight ->
        StandingRight

      Climbing _ ->
        Climbing Nothing

      Falling sp ->
        -- this shouldn't happen, yet here we are
        Falling sp

      IdleFall ->
        IdleFall

      Sitting ->
        Sitting
      
      Watering ToRight ->
        StandingRight
      
      Watering ToLeft ->
        StandingLeft
  , state ^. gameDataMap
  )


water :: (RandomGen g) => GameData -> Rand g (Double, Double, PlayerState, Level)
water state = do
  newLevel <- addPlant (state ^. gameDataMap)
                       (state ^. gameDataPlayer . playerLocation . _1
                       , state ^. gameDataPlayer . playerLocation . _2)
                       direction
  return ( state ^. gameDataPlayer . playerLocation . _1
         , state ^. gameDataPlayer . playerLocation . _2
         , Watering direction
         , newLevel
         )
  where
    direction = case state ^. gameDataPlayer . playerState of
      StandingLeft ->
        ToLeft
        
      StandingRight ->
        ToRight

      Walking dir ->
        dir

      Watering dir ->
        dir

      _ ->
        ToRight


addPlant :: (RandomGen g) => Level -> (Double, Double) -> HDirection -> Rand g Level
addPlant level (x, y) dir = do
  let xd = x `div'` 16
  let yd = y `div'` 16
  let pxd = case dir of
              ToLeft ->
                xd - 1

              ToRight ->
                xd + 1

  -- 99  -- tentacle 
  -- 111 -- tree top
  -- 117 -- tree bottom
  -- 87  -- yellow tentacles
  -- 105 -- mushroom

  let grassIds = [ 81, 93 ]
  let plantIds = [ 87, 99, 105, 117 ]
  gId <- randomItem grassIds
  pId <- randomItem plantIds

  let rpId = if canAddPlant level pxd yd
              then
                Just pId
              else
                Nothing

  let rGid = if canAddGrass level pxd yd
              then
                Just gId
              else
                Nothing

  let newLevel = 
        level & levelPlants %~ (\ps -> 
                                  case rpId of
                                    Nothing ->
                                      ps
                                      
                                    Just i ->
                                      if i == 117
                                        then
                                          U.update ps (U.fromList [ ((yd - 1) * 60 + pxd, 111)
                                                                  , (yd * 60 + pxd, i)
                                                                  ])
                                        else
                                          U.update ps (U.fromList [(yd * 60 + pxd, i)]))

              & levelGrass %~ (\gs ->
                                  case rGid of
                                    Nothing ->
                                      gs
                                    
                                    Just i ->
                                      U.update gs (U.fromList [(yd * 60 + pxd, i)]))

  return newLevel


canAddGrass :: Level -> Int -> Int -> Bool
canAddGrass level x y =
  tileAt level x y `elem` [emptyTile, risingRamp, fallingRamp]
  && grassAt level x y == 0
  && not (tileAt level x (y + 1) `elem` [emptyTile, risingRamp, fallingRamp])


canAddPlant :: Level -> Int -> Int -> Bool
canAddPlant level x y =
  tileAt level x y `elem` [emptyTile, risingRamp, fallingRamp]
  && plantAt level x y == 0
  && not (tileAt level x (y + 1) `elem` [emptyTile, risingRamp, fallingRamp])


randomItem :: (RandomGen g) => [a] -> Rand g a
randomItem items = do
  let n = length items
  i <- getRandomR (0, n - 1)
  return $ items L'.!! i


backToMenu :: GameData -> Word32 -> GameState
backToMenu gameData tick =
  MainMenu $ MainMenuData
    { _mainMenuDataParallax       = gameData ^. gameDataParallax
    , _mainMenuDataBackgroundTick = gameData ^. gameDataBackgroundTick
    , _mainMenuDataTick           = tick
    , _mainMenuDataMaps           = gameData ^. gameDataDefaultMaps
    }


renderMainMenu :: MainMenuData -> RIO App ()
renderMainMenu menuData = do
  renderer <- asks appRenderer
  font <- asks (_texturesFont . appTextures)
  clear renderer
  renderParallax $ menuData ^. mainMenuDataParallax
  renderMap (menuData ^. mainMenuDataBackgroundTick)
            (menuData ^. mainMenuDataMaps . defaultMapsTitle)
            Nothing

  rendererDrawColor renderer $= V4 0 0 0 255
  loadingSurface <- Font.solid font (V4 250 250 250 255) "Press start"
  loading <- createTextureFromSurface renderer loadingSurface
  loadingInfo <- queryTexture loading
  let w = textureWidth loadingInfo
  let h = textureHeight loadingInfo
  copy renderer loading Nothing (Just (Rectangle (P $ V2 (480 - (w `div` 2)) (520 - (w `div` 2))) (V2 w h)))

  present renderer


renderGameRunning :: GameData -> RIO App ()
renderGameRunning gameData = do
  renderer <- asks appRenderer
  clear renderer
  renderParallax $ gameData ^. gameDataParallax
  renderMap (gameData ^. gameDataBackgroundTick)
            (gameData ^. gameDataMap)
            (Just $ gameData ^. gameDataPlayer)

  present renderer




quitRequested :: Event -> Bool
quitRequested event =
  case eventPayload event of
    WindowClosedEvent _ ->
      True

    QuitEvent ->
      True

    _ ->
      False


updateParallax :: (RandomGen g) => Word32 -> [BgObject] -> Rand g [BgObject]
updateParallax dt =
  mapM (updateSingleParallax dt')
  where
    dt' = fromIntegral dt


updateSingleParallax :: (RandomGen g) => Double -> BgObject -> Rand g BgObject
updateSingleParallax dt obj = do
  if newX < -16
    then
    -- place some of the new stars little bit further away
      star (960, 1000)
    else
      return $ obj & bgObjectLocation . _1 .~ newX
  where
    newX = (obj ^. bgObjectLocation . _1) + (obj ^. bgObjectSpeed) * dt / 1000


renderMap :: Word32 -> Level -> Maybe Player -> RIO App ()
renderMap tick level player = do
  let frame = (tick `rem` 1200) `quot` 200
  renderLayer (level ^. levelTextures) (level ^. levelBackground)  
  renderLayer (level ^. levelTextures) (level ^. levelLadders)
  renderAnimatedLayer (fromIntegral frame) (level ^. levelTextures) (level ^. levelPlants)
  renderLayer (level ^. levelTextures) (level ^. levelForeground)
  renderPlayer tick player
  renderLayer (level ^. levelTextures) (level ^. levelFences)
  renderAnimatedLayer (fromIntegral frame) (level ^. levelTextures) (level ^. levelGrass)


renderPlayer :: Word32 -> Maybe Player -> RIO App ()
renderPlayer _ Nothing =
  return ()

renderPlayer t (Just player) = do
  texture <- asks (_texturesPlayer . appTextures)
  spray <- asks (_texturesSpray . appTextures)
  renderer <- asks appRenderer
  let (x, y) = player ^. playerLocation

  copy renderer
       texture
       (playerTextureSource t player)
       (Just (Rectangle (P (V2 (truncate (x - 8)) (truncate y - 31)))
                        (V2 16 32)))

  case player ^. playerState of
    Watering dir ->
      let
        -- 10 frames
        frame = ((player ^. playerAnimStarted - t) `rem` (10 * 100)) `quot` 100
      in
        case dir of
          ToLeft ->
            copy renderer
                 spray
                 (Just (Rectangle (P (V2 (fromIntegral frame * 16) 16)) (V2 16 16)))
                 (Just (Rectangle (P (V2 (truncate (x - 24)) (truncate y - 15)))
                                  (V2 16 16)))

          ToRight ->
            copy renderer
                 spray
                 (Just (Rectangle (P (V2 (fromIntegral frame * 16) 0)) (V2 16 16)))
                 (Just (Rectangle (P (V2 (truncate (x + 8)) (truncate y - 15)))
                                  (V2 16 16)))


    _ ->
      return ()


playerTextureSource :: Word32 -> Player -> Maybe (Rectangle CInt)
playerTextureSource tick player =
  case player ^. playerState of
    StandingLeft ->
      Just (Rectangle (P (V2 0 64)) (V2 16 32))

    StandingRight ->
      Just (Rectangle (P (V2 0 0)) (V2 16 32))

    Walking dir ->
      let
        -- 8 frames
        frame = ((player ^. playerAnimStarted - tick) `rem` (8 * 100)) `quot` 100
      in
        case dir of
          ToLeft ->
            Just (Rectangle (P (V2 (fromIntegral frame * 16) 96)) (V2 16 32))

          ToRight ->
            Just (Rectangle (P (V2 (fromIntegral frame * 16) 32)) (V2 16 32))

    Climbing dir ->
      let
        -- 8 frames
        frame = ((player ^. playerAnimStarted - tick) `rem` (8 * 60)) `quot` 60
      in
        case dir of
          Nothing ->
            Just (Rectangle (P (V2 0 128)) (V2 16 32))
          Just _ ->
            Just (Rectangle (P (V2 (fromIntegral frame * 16) 160)) (V2 16 32))

    Falling start ->
      let
        -- 4 frames
        frame = ((player ^. playerAnimStarted - tick) `rem` (4 * 60)) `quot` 60
        dist = player ^. playerLocation . _2  - start
      in
        if dist <= 32
          then
            Just (Rectangle (P (V2 0 192)) (V2 16 32))
          else
            Just (Rectangle (P (V2 (fromIntegral frame * 16) 224)) (V2 16 32))

    IdleFall ->
      Just (Rectangle (P (V2 0 192)) (V2 16 32))

    Sitting ->
      let
        -- 4 frames
        frame = ((player ^. playerAnimStarted - tick) `rem` (4 * 150)) `quot` 150
      in
        Just (Rectangle (P (V2 (fromIntegral frame * 16) 256)) (V2 16 32))

    Watering ToLeft ->
      Just (Rectangle (P (V2 0 320)) (V2 16 32))

    Watering ToRight ->
      Just (Rectangle (P (V2 0 288)) (V2 16 32))


renderAnimatedLayer :: Int -> [(LowerBound, UpperBound, ColumnCount, Texture)] -> U.Vector Int -> RIO App ()
renderAnimatedLayer frame textures layer = do
  let width = [0..59]
  let height = [0..32]

  mapM_ (\x -> mapM_
          (\y -> do
            let tile = layer U.! (x + y * 60)
            when (tile /= 0)
                (renderTile textures (tile + frame) (fromIntegral x) (fromIntegral y)))
          height)
        width


renderLayer :: [(LowerBound, UpperBound, ColumnCount, Texture)] -> U.Vector Int -> RIO App ()
renderLayer textures layer = do
  let width = [0..59]
  let height = [0..32]

  mapM_ (\x -> mapM_ (\y -> renderTile textures (layer U.! (x + y * 60)) (fromIntegral x) (fromIntegral y)) height) width


renderTile :: [(LowerBound, UpperBound, ColumnCount, Texture)] -> Int -> CInt -> CInt -> RIO App ()
renderTile _ 0 _ _ =
  return ()


renderTile textures tile x y = do
  let textureMatch = L.find (\(lower, upper, _, _) -> _unLowerBound lower <= tile && _unUpperBound upper > tile) textures

  case textureMatch of
    Just (lowerBound, _, cCount, texture) -> do
      renderer <- asks appRenderer

      let (q, r) = (tile - _unLowerBound lowerBound) `quotRem` _unColumnCount cCount
      let source = Just (Rectangle (P (V2 (fromIntegral r * 16) (fromIntegral q * 16))) (V2 16 16))

      copy renderer
           texture
           source
           (Just (Rectangle (P (V2 (x * 16)
                                   (y * 16)))
                               (V2 16 16)))

    Nothing -> do
      logError ("failed to render tile: " <> displayShow tile)


renderParallax :: [BgObject] -> RIO App ()
renderParallax objs = do
  texture <- asks (_texturesStars . appTextures)
  renderer <- asks appRenderer
  mapM_ (renderStar renderer texture) objs

  where
    renderStar renderer texture obj = do
      copy renderer
           texture
           (Just (Rectangle (P (V2 (16 * (obj ^. bgObjectTextureIndex)) 0)) (V2 16 16)))
           (Just (Rectangle (P (V2 (round $ obj ^. bgObjectLocation . _1)
                                   (round $ obj ^. bgObjectLocation . _2)))
                            (V2 16 16)))



setupParallax :: (RandomGen g) => Rand g [BgObject]
setupParallax =
  replicateM 200 (star (0, 1080))


star :: (RandomGen g) => (Double, Double) -> Rand g BgObject
star range = do
  x <- getRandomR range -- (0, 1080)
  y <- getRandomR (0, 946)
  index <- getRandomR (0, 11)
  speed <- getRandomR (-20, -200)

  return $ BgObject
            { _bgObjectTextureIndex = index
            , _bgObjectSpeed        = speed
            , _bgObjectLocation     = (x, y)
            }
