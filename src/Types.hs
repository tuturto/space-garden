{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Types where

import RIO
import RIO.Process

import qualified Data.Vector.Unboxed as U
import Foreign.C.Types
import Lens.Micro.TH ( makeLenses )
import SDL
import qualified SDL.Font as Font


-- | Command line arguments
data Options = Options
  { optionsVerbose     :: !Bool
  , optionsDevelopment :: !Bool
  }


data App = App
  { appLogFunc        :: !LogFunc
  , appProcessContext :: !ProcessContext
  , appOptions        :: !Options
  , appTextures       :: !Textures
  , appRenderer       :: !Renderer
  }


instance HasLogFunc App where
  logFuncL = lens appLogFunc (\x y -> x { appLogFunc = y })
instance HasProcessContext App where
  processContextL = lens appProcessContext (\x y -> x { appProcessContext = y })


data GameState
  = MainMenu !MainMenuData
  | GameRunning !GameData
  | GameExiting


data MainMenuData = MainMenuData
  { _mainMenuDataParallax       :: ![BgObject]
  -- | background tick is used to update parallax animation
  -- | It needs to be done only once per rendered frame
  , _mainMenuDataBackgroundTick :: !Word32
  -- | tick is used to update actors
  -- | It needs to be updated for each received event
  , _mainMenuDataTick           :: !Word32
  , _mainMenuDataMaps           :: !DefaultMaps
  }


data DefaultMaps = DefaultMaps
  { _defaultMapsTitle :: !Level
  , _defaultMapsGame  :: !Level
  }


data GameData = GameData
  { _gameDataParallax       :: ![BgObject]
  , _gameDataTick           :: !Word32
  , _gameDataBackgroundTick :: !Word32
  , _gameDataMap            :: !Level
  , _gameDataDefaultMaps    :: !DefaultMaps
  , _gameDataPlayer         :: !Player
  , _gameDataControlState   :: !ControlState
  }


data Textures = Textures
  { _texturesStars       :: !Texture
  , _texturesTiles       :: ![(FilePath, Texture)]
  , _texturesPlayer      :: !Texture
  , _texturesFont        :: !Font.Font
  , _texturesSpray       :: !Texture
  }


data BgObject = BgObject
  { _bgObjectTextureIndex :: !CInt
  , _bgObjectSpeed        :: !Double
  , _bgObjectLocation     :: !(Double, Double)
  }


data Level = Level
  { _levelName       :: !Text
  , _levelWidth      :: !Int
  , _levelHeight     :: !Int
  , _levelFences     :: !(U.Vector Int)
  , _levelGrass      :: !(U.Vector Int)
  , _levelPlants     :: !(U.Vector Int)
  , _levelLadders    :: !(U.Vector Int)
  , _levelForeground :: !(U.Vector Int)
  , _levelBackground :: !(U.Vector Int)
  , _levelTextures   :: ![(LowerBound, UpperBound, ColumnCount, Texture)]
  }


data PlayerState =
  StandingLeft
  | StandingRight
  | Walking !HDirection
  | Climbing !(Maybe VDirection)
  | Falling !Double
  | IdleFall
  | Sitting
  | Watering !HDirection
  deriving (Eq)


data Direction =
  Direction (Maybe VDirection) (Maybe HDirection)
  deriving (Eq)


data VDirection =
  ToUp
  | ToDown
  deriving (Eq)


data HDirection =
  ToLeft
  | ToRight
  deriving (Eq)


data Player = Player
  { _playerLocation    :: !(Double, Double)
  , _playerState       :: !PlayerState
  , _playerAnimStarted :: !Word32
  }


data ControlState = ControlState
  { _controlStateRight :: !Bool
  , _controlStateLeft  :: !Bool
  , _controlStateUp    :: !Bool
  , _controlStateDown  :: !Bool
  , _controlStateWater :: !Bool
  }


newtype LowerBound = MkLowerBound { _unLowerBound :: Int }
  deriving (Show, Read, Eq, Ord, Num )

newtype UpperBound = MkUpperBound { _unUpperBound :: Int }
  deriving (Show, Read, Eq, Ord, Num )

newtype ColumnCount = MkColumnCount { _unColumnCount :: Int }
  deriving (Show, Read, Eq, Ord, Num )

makeLenses ''Textures
makeLenses ''BgObject
makeLenses ''GameData
makeLenses ''MainMenuData
makeLenses ''DefaultMaps
makeLenses ''Level
makeLenses ''LowerBound
makeLenses ''UpperBound
makeLenses ''ColumnCount
makeLenses ''Player
makeLenses ''ControlState
