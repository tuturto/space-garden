{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Run (run) where

import Import
import RIO.List as L
import RIO.Text as T

import SDL

import Control.Monad.Random ( evalRandIO )
import System.Directory ( listDirectory )
import System.FilePath

import Data.Aeson
import Data.Either.Extra
import Tiled
import UserInterface


run :: RIO App ()
run = do
  logInfo "Loading levels..."
  t <- ticks
  textures <- asks (_texturesTiles . appTextures)

  tilesetsE <- loadTilesets :: RIO App (Either [String] [(FilePath, Tileset)])

  case tilesetsE of
    Right tilesets -> do
      titleMap <- loadMap textures tilesets "title-map.json"
      gameMap <- loadMap textures tilesets "game-map.json"

      let mapsM = DefaultMaps <$> titleMap <*> gameMap

      case mapsM of
        Right maps -> do
          parallax <- liftIO $ evalRandIO setupParallax
          let state = MainMenu $ MainMenuData
                                  { _mainMenuDataParallax       = parallax
                                  , _mainMenuDataBackgroundTick = t
                                  , _mainMenuDataTick           = t
                                  , _mainMenuDataMaps           = maps
                                  }

          logInfo "Entering main loop..."
          _ <- appLoop state
          logInfo "Main loop finished"
        
        Left err ->
          logError $ displayShow ("Initialization failed (" <> err <> "), exiting...")

    Left errs -> do
      logError $ displayShow ("Initialization failed: (" <> L.intercalate ", " errs <> ").")


appLoop :: GameState -> RIO App GameState
appLoop state = do
  render state
  pumpEvents
  events <- pollEvents
  userInput <- foldM handleEvents state $ sortBy (\a b -> compare (eventTimestamp a) (eventTimestamp b)) events
  t <- ticks
  newState <- update t userInput
  case newState of
    GameExiting -> 
      return newState

    _ -> 
      appLoop newState


loadMap :: [(FilePath, Texture)] -> [(FilePath, Tileset)] -> FilePath -> RIO App (Either String Level)
loadMap textures tilesets fp = do
  mapJson <- loadMapJSON fp 
  return $ mapJson >>= tiledToLevel name textures tilesets
  where
    -- TODO: proper name
    name = T.pack $ takeBaseName fp


-- | Combine loaded textures, tilesets and tiled map into level
-- | name is name of the level
-- | textures has list of (Text, Texture), where first element is name of the texture and second is the actual texture
-- | tSets is list of TileSet
-- | tMap is TiledMap that is being converted to Level
tiledToLevel :: Text -> [(FilePath, Texture)] -> [(FilePath, Tileset)] -> TiledMap -> Either String Level
tiledToLevel name textures tSets tMap = 
  Level <$> Right name
        <*> tMap ^. tiledMapWidth . to Right
        <*> tMap ^. tiledMapHeight . to Right
        <*> (_tiledLayerData <$> layer "fences")
        <*> (_tiledLayerData <$> layer "grass")
        <*> (_tiledLayerData <$> layer "plants")
        <*> (_tiledLayerData <$> layer "ladders")
        <*> (_tiledLayerData <$> layer "foreground")
        <*> (_tiledLayerData <$> layer "background")
        <*> textureSets
  where
    layer n = maybeToEither ("Layer " <> show n <> "not found") $ L.find (\x -> _tiledLayerName x == n) (tMap ^. tiledMapLayers)
    textureSets = case partitionEithers (textureTilesetPair textures tSets <$> (tMap ^. tiledMapTilesets)) of
                    (errs@(_:_), _) ->
                      Left $ L.intercalate ", " errs

                    (_, rs) ->
                      Right rs
    

-- | Build texture information for given tileset link
textureTilesetPair :: [(FilePath, Texture)] -> [(FilePath, Tileset)] -> TilesetLink -> Either String (LowerBound, UpperBound, ColumnCount, Texture) 
textureTilesetPair textures tilesets tLink = 
  case (tilesetMatch, textureMatch) of
    (Just (_, tileset), Just (_, texture)) ->
      Right ( tLink ^. tilesetLinkFirstGid . to MkLowerBound
            , MkUpperBound (tileset ^. tilesetTileCount + tLink ^. tilesetLinkFirstGid)
            , tileset ^. tilesetColumns . to MkColumnCount
            , texture
            )

    (Nothing, _) ->
      Left "Tileset not found"

    (Just (_, tileset), Nothing) ->
      Left $ T.unpack ("Texture not found '" <> tileset ^. tilesetImage <> "'")

  where
    tilesetMatch = L.find (\(path, _) -> takeFileName path == tLink ^. tilesetLinkSource . to (takeFileName . T.unpack)) tilesets
    textureMatch = L.find (\(path, _) -> 
                    Just (takeFileName path) == ((\(_, t) -> t ^. tilesetImage . to (takeFileName . T.unpack)) <$> tilesetMatch)) textures


loadTilesets :: RIO App (Either [String] [(FilePath, Tileset)])
loadTilesets = do
  files <- liftIO $ listDirectory "assets/tilesets/"
  let jsonFiles = L.filter (\f -> takeExtension f == ".json") files
  jsons <- mapM (\path -> do
                            logDebug $ displayShow ("loading file '" <> path <> "'...")
                            res <- readFileBinary ("assets/tilesets/" <> path)
                            return (path, res)) jsonFiles
  let tSets = (\(path, content) -> (path, eitherDecodeStrict content)) <$> jsons :: [(FilePath, Either String Tileset)]
  let (errs, res) = partitionEithers $ snd <$> tSets

  if L.null errs
    then
      return $ Right (L.zip (fst <$> tSets) res)

    else
      return $ Left errs


loadMapJSON :: FilePath -> RIO App (Either String TiledMap)
loadMapJSON path = do
  logDebug $ displayShow ("loading file '" <> path <> "'...")
  content <- readFileBinary ("assets/maps/" <> path)
  return $ eitherDecodeStrict content
